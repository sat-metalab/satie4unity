﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if (UNITY_EDITOR) 
using UnityEditor;
#endif


[ExecuteInEditMode]
public class satieUtilities : MonoBehaviour
{



#if (UNITY_EDITOR) 
    [MenuItem("SATIE/Update Selected")]
    static void updateSelectedSources()
    {
        if (!Application.isPlaying)   // edit mode only
        {
            List <SATIEsource> sources = new List <SATIEsource>();

            GameObject[] gameObjects = Selection.gameObjects;
            foreach (GameObject gob in gameObjects)
            {
                SATIEsource[] sourcesCS;
                sourcesCS = gob.GetComponentsInChildren<SATIEsource>();
                foreach (SATIEsource sourceCS in sourcesCS)
                    updateNode((SATIEnode)sourceCS);
            }
        }
    }

    // Use this for initialization
    [MenuItem("SATIE/Update All Sources In Current Scene")]
#endif

    static void updateSatieSources()
    {
		
        if (!Application.isPlaying)   // edit mode only
        {
            SATIEsource[] srcNodesCS = FindObjectsOfType(typeof(SATIEsource)) as SATIEsource[];
            foreach (SATIEsource srcNode in srcNodesCS)
                updateNode((SATIEnode)srcNode);
        }
    }


    public static void updateNode(SATIEnode satieNode)
    {
        string uriString= satieNode.uri;
        string pluginString = "";

        // this is a legacy check for backwards compatibility
        if (uriString.Contains("process://"))
        {
            //                isProcess = true;
            satieNode.satiePluginType = SATIEnode.satiePluginTypes.process;
            pluginString = uriString.Substring(10);  // backwards compatibility: prune URI header, leaving only the name of the process and any possible args built in the string
        } 
        else if (uriString.Contains("effect://"))
        {
            satieNode.satiePluginType = SATIEnode.satiePluginTypes.effect;
            string str = uriString.Substring(9);  // backwards compatibility: prune URI header, leaving only the name of the process and any possible args built in the string

            if (str.Contains("inBus"))
            {
                int index = str.IndexOf("inBus");
                pluginString = str.Remove(index, 5);
            } else
                pluginString = str;
        } 
        else if (uriString.Contains("plugin://"))
        {
            satieNode.satiePluginType = SATIEnode.satiePluginTypes.sound;
            pluginString = uriString.Substring(9);  // backwards compatibility: prune URI header, leaving only the name of the process and any possible args built in the string
        }
        else 
            pluginString = uriString;


        if (pluginString == "")
        {
            switch (satieNode.satiePluginType)
            {
                case SATIEnode.satiePluginTypes.sound :
                    pluginString = "default";
                    break;
                case SATIEnode.satiePluginTypes.effect :
                    pluginString = "defaultFx";
                    break;
                case SATIEnode.satiePluginTypes.process :
                    pluginString = "defaultProcess";
                    break;
            }
        }
      

        satieNode.plugin.name = pluginString;

        satieNode.uri = "";   // eliminate this param.. will be depreciated in future

        if (satieNode.PropertyMessages.Count == 0) return; // nothing to do 

        List<satieNodeProperty> props = new List<satieNodeProperty>();

        foreach (string propStr in satieNode.PropertyMessages)
        {
            string[] keyVals = propStr.Split(' ');
            string key = "";
            string val = "";

            satieNodeProperty nodeProp = new satieNodeProperty();

            foreach (string s in keyVals)
            {
                if (key.Length == 0 && s != " ")
                    key = s;
                else if (val.Length == 0 && s != " ")
                    val = s;
            }

            if (key.Length == 0)
                Debug.LogError("satieUtilities.updateNode(): empty key substring found in node: " + satieNode.name);
            if (val.Length == 0)
                Debug.LogError("satieUtilities.updateNode(): empty value substring found in node: " + satieNode.name);

            if (key.Length == 0 || val.Length == 0)
                break;

            nodeProp.name = key;
            nodeProp.value = val;
            props.Add(nodeProp);
            //Debug.Log("satieUtilities.updateNode(): Node " + satieNode.name + "  propStr: " + propStr);
        }
        // if (srcNode.nodePropertyList.Count > 0)  srcNode.PropertyMessages.Clear;  enable this when things are stable
        satieNode.setProperties(props);
        satieNode.PropertyMessages.Clear();
    }



    // Update is called once per frame
    void Update()
    {
		
    }

    void OnEnable()
    {

    }

    void OnDisable()
    {
    }

}
