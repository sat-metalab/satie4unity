// Satie4Unity, audio rendering support for Unity
// Copyright (C) 2016  Zack Settel

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
// -----------------------------------------------------------
using UnityEngine.UI;
using UnityEngine;
//#if (UNITY_EDITOR) 
//using UnityEditor;
//#endif
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Runtime.InteropServices;
using System.Net;
using System.Net.Sockets;
//using OSC.NET;


//using OscSimpl;

[ExecuteInEditMode]
public class SATIEsetup : MonoBehaviour
{
   
    public  enum satieQueryTypes          { audiosource, effect, mapper, spatializer, postprocessor, process, audiosourceArgs, satieQueryTypesCount }   // satieQueryTypesCount has to be last !!
    public static string[] satieQueryTypeNames = {"audiosource", "effect", "mapper", "spatializer", "postprocessor", "process", "audiosourceArgs"};    // this array must be synchronized with the above !!

 
    // public GameObject uiWrapper;

    //public bool enabled = true;
    public static bool connected = false;

    public bool isConnected()
    {
        return connected;
    }

    //private static OSCTransmitter sender;

    public static OscOut oscOutNode;
    //public static OscIn oscInNode;

    //private static OscMessage uBlobSrcMess;
    //private static OscMessage uBlobProcMess;

    public static OscBundle bundle;

    private string myIP;
    public string RendererAddress = "127.0.0.1";
    private string _RendererAddress;
    public int RendererPort = 18032;
    public int satieQueryPort = 18042;

 
    static public string getAddress()
    {
        return _instance.RendererAddress;
    }

    static public int getQueryPort()
    {
        return _instance.satieQueryPort;
    }

    static public int getTXport()
    {
        return _instance.RendererPort;
    }

//    public OscOut getOscOut()
//    {
//        return oscOutNode;
//    }

//    public OscIn getOscIn()
//    {
//        return oscInNode;
//    }
        


    public bool useFixedUpdate = false;

    public float updateRateMs = 30;
    private float _updateRateMs;

//    [Tooltip("sends node updates using osc blobs, with lower rez params") ]
    private bool enableOscBlobTX = false;    // unstable feature disabled

    public bool invertAzimuth = false;
    public float azimuthOffset = 0f;
    private float _azimuthOffset;


    public static bool invertAzi = false;
    public static float aziOffset = 0f;
    // this is referenced by source connections


    public static bool debugMessTx_static = false;
    public bool debugMessTx = false;


//    const string uBlobSrcAddress = "/satie/source/ublob";
//    const string uBlobProcAddress = "/satie/process/ublob";

    [HideInInspector] 
    public static float updateRateSecs = .02f;
         
    // list of all instantiated nodes
    [HideInInspector] 
    public  static List<SATIEnode> SATIEnodeList = new List<SATIEnode>();

    private bool _enabled = false;

    public static bool OSCenabled { get { return _instance._enabled; } }

    public static bool updateBlobEnabled { get { return _instance.enableOscBlobTX; } }


    //private static readonly SATIEsetup _instance = new SATIEsetup();

    private static  SATIEsetup _instance = null;

    public static SATIEsetup Instance { get { return _instance; } }

    public SATIEsetup()
    {       
    }

    Text fpsText = null;
    // used to display FPS

    private int frameCount = 0;
    private float dt = 0.0f;
    private float fps = 0.0f;
    private float updateRate = 4.0f;
    // 4 updates per sec.


    // set up translator(s)  for now, using only the basic translator
    void Awake()
    {
//        if (!Application.isPlaying) 
//        {
//            Debug.Log(transform.name+"  "+GetType()+".Awake(): EDIT MODE");
//        }
//        else 
//            Debug.Log(transform.name+"  "+GetType()+".Awake(): PLAY MODE");

        if (_instance != null)
        {
            Debug.LogWarning(transform.name+"  "+GetType()+".Awake(): multiple instances of SATIEsetup not allowed, duplicate instance found in transform");
            return;
        }

        _instance = this;    // force singleton

 
        if (RendererAddress.Equals("localhost"))
            RendererAddress = "127.0.0.1";

        _RendererAddress = RendererAddress;

        myIP = LocalIPaddress();
        Debug.Log(transform.name+"  "+GetType()+".Awake: MY IP: " + myIP+" oscTX:" + RendererAddress + ":" + RendererPort); //+"  oscRX port:"+ satieQueryPort);

        bundle = new OscBundle();

        setUpdateRate(updateRateMs);

        if (! oscConnect())
        {
                Debug.LogError(transform.name+"  "+GetType()+".Awake:  can't connect to OSC object(s), check osc components in transform");
            //Destroy(this);
        }
        else 
            _instance._enabled = true;
    }
	
    void Start()
    {
//        if (!Application.isPlaying) 
//            Debug.Log(GetType()+".Start(): EDIT MODE " + transform.name);
//        else if (Application.isPlaying)
//            Debug.Log(GetType()+".Start(): PLAY MODE " + transform.name);
//        else  Debug.Log(GetType()+".Start(): UNRECOGNIZED MODE " + transform.name);

        //if (Application.isEditor) return;


        GameObject obj = GameObject.Find("FPStext");

        if (obj != null)
            fpsText = obj.GetComponent<Text>();

        invertAzi = invertAzimuth;
        aziOffset = (_azimuthOffset = azimuthOffset)*Mathf.Deg2Rad;

        debugMessTx_static = debugMessTx;

        if (Application.isPlaying) 
            StartCoroutine(initSatie());
    }
	
    void OnValidate()
    {
        if (!connected)
            return;
        if (_RendererAddress != RendererAddress)
        {
            oscReconnect();
        }
        if (_updateRateMs != updateRateMs)
            setUpdateRate(updateRateMs);

        if (invertAzi != invertAzimuth)
            invertAzi = invertAzimuth;

        if (_azimuthOffset != azimuthOffset)
            aziOffset = (_azimuthOffset = azimuthOffset)*Mathf.Deg2Rad;
        

        if (debugMessTx_static != debugMessTx)
            debugMessTx_static = debugMessTx;

        if (RendererAddress.Equals("localhost"))
            RendererAddress = "127.0.0.1";
    }

    void setUpdateRate(float updateMs)
    {
        _updateRateMs = updateMs;
        updateRateSecs = updateMs / 1000f;
    }

    IEnumerator initSatie() // now that litener(s) have been conection related parameters.
    {
        yield return new WaitForSeconds(0.2f);   // may need to be bigger for large scenes
        refreshNodes();    // or do this using a coroutine to avoid OSC peaking
    }

    public void refreshNodes()
    {
        foreach (SATIEnode  node in SATIEnodeList)
            node.refreshState();
    }

    // connections are serviced after the previous frame's physics engine is updated during lateUpdate()
    public virtual void FixedUpdate()
    {
        if (useFixedUpdate)
            serviceConnections(); 
    }
	
    // Update is called once per frame
    public virtual void Update()
    {
        frameCount++;
        dt += Time.deltaTime;
        if (dt > 1.0f / updateRate)
        {
            fps = frameCount / dt;
            frameCount = 0;
            dt -= 1.0f / updateRate;
        }
        if (fpsText)
            fpsText.text = ((int)fps).ToString() + " fps, frameDurMS: " + ((int)(Time.deltaTime * 1000f)).ToString();
        if (!useFixedUpdate)
            serviceConnections();    
    }

    private float _lastUpdate;

    void serviceConnections()
    {
        if (!connected)
            return;

        if (Time.time - _lastUpdate > updateRateSecs)
        {
            // service all nodes
            foreach (SATIEnode node in SATIEnode.sourceInsatances)
            {
                SATIEsource src = (SATIEsource)node;
                src.evalConnections();
            }
            // reset listener flags
            foreach (SATIEnode node in SATIEnode.listenerInsatances)
            {
                node.updatePosFlag = node.updateRotFlag = false;
            }
            _lastUpdate = Time.time;
        }
    }

    public void freeSrcNodesInGroup(string groupName)
    {
        if (!connected)
            return;

        // iterate backwards to modify collection
        for (int i = SATIEnode.sourceInsatances.Count - 1; i >= 0; i--)
        {           
            SATIEsource src = (SATIEsource)SATIEnode.sourceInsatances[i];
                         
            if (src.group.Equals(groupName))
            {
                Debug.Log(GetType() + ".freeSrcNodesInGroup():   deleting node: " + src.nodeName + "  in group: " + src.group);                 
                SATIEnode.sourceInsatances.RemoveAt(i);
                src.deleteNode(src.nodeName);                 
            }
        }

    }

    void OnDestroy()
    {
    }

    void OnEnable()
    {
//        if (!Application.isPlaying) 
//            Debug.Log(transform.name+" "+GetType()+".OnEnable(): EDIT MODE");
//        else if (Application.isPlaying)
//            Debug.Log(transform.name+" "+GetType()+".OnEnable(): PLAY MODE");
//        else  Debug.Log(transform.name+" "+GetType()+".OnEnable(): UNRECOGNIZED MODE");
        //Debug.Log(transform.name+":  "+GetType()+".OnEnable(): TOTAL NODES LOADED IN SCENE " + SATIEnodeList.Count);  
     }

    void OnDisable()
    {
//        if (!Application.isPlaying) 
//            Debug.Log(transform.name+" "+GetType()+".OnDisable(): EDIT MODE");
//        else if (Application.isPlaying)
//            Debug.Log(transform.name+" "+GetType()+".OnDisable(): PLAY MODE");
//        else  Debug.Log(transform.name+" "+GetType()+".OnDisable(): UNRECOGNIZED MODE");
    }


    public void OnApplicationQuit()
    {

        if (!Application.isPlaying) return;

        OscMessage message = new OscMessage("/satie/scene/clear");

                Debug.Log(transform.name+"  "+GetType()+".OnDestroy");

        //message.Add("clear");
        sendOSC(message);

        oscDisconnect();   
    }


    bool oscConnect()
    {
        return  doOSCconnect(false);
    }

    bool oscReconnect()
    {
        return  doOSCconnect(true);
    }

    bool doOSCconnect(bool reconnectFlag)
    {
       // Debug.Log(".oscConnect()");

        if ( connected && !reconnectFlag )
        {
            Debug.Log(transform.name+"  "+GetType()+".oscConnect(): OSC already enabled, open status:  oscTX:"+oscOutNode.isOpen);
            return (connected);
        }
        //Debug.Log(GetType()+".oscConnect");
        connected = false;
        if (oscOutNode == null ) //|| !oscOutNode.isOpen)
        {
            oscOutNode = gameObject.GetComponent<OscOut>();
            if (oscOutNode == null)
            {
                oscOutNode = gameObject.AddComponent<OscOut>();
            }
        }
        if (!oscOutNode.isOpen || reconnectFlag ) oscOutNode.Open(RendererPort,RendererAddress );

        connected = oscOutNode; //!= null  && oscInNode != null;

        if (connected)
            Debug.Log(transform.name+"  "+GetType()+"oscConnect(): open status:  oscTX:"+oscOutNode.isOpen); //+"  oscRX:"+oscInNode.isOpen);
        else
            Debug.LogError(transform.name+"  "+GetType()+"oscConnect(): bad open statu(s):  oscTX:"+oscOutNode.isOpen); //+"  oscRX:"+oscInNode.isOpen);

        return (connected);
    }

    public void oscDisconnect()
    {

        if (!enabled) return;

        if (oscOutNode.isOpen)
        {
            oscOutNode.Close();
        }
        oscOutNode = null;
        connected = false;
        Debug.Log(transform.name+"  "+GetType()+"oscDisconnect(): Closing OSX");
    }
    

    // expand this to bundle N messages during a certain delta time T;
    public static bool sendOSC(OscMessage mess)
    {
        bool status;

        if (oscOutNode == null)
        {
            Debug.LogWarning("SATIEsetup.sendOSC():  OSCout object not found, skipping message: " + mess.address);
            return false;
        }

        if (!oscOutNode.isOpen)
        {
            Debug.LogError("SATIEsetup.sendOSC():  OSCout object is closed, skipping message: " + mess.address);    
            return false;
        }

        // int bytesSent = 0;
        bundle.Add(mess);
        // bundle.Add( blobMessage );
        status = oscOutNode.Send(bundle);

        bundle.Clear();

        return status;
    }


    //    /satie/scene createSource  nodeName  synthDefName<uriPath>   groupName<opt>
    public static bool createSource(string nodeName, string uriString, string groupName)
    {
        bool result;
        const string path = "/satie/scene/createSource";
        //OSCMessage message = new OSCMessage(path);
        OscMessage creationMess = new OscMessage(path);
        creationMess.Add(nodeName);
        creationMess.Add(uriString);
        creationMess.Add(groupName);

        result = sendOSC(creationMess);

        return result;
    }

    //    /satie/scene createSource  nodeName  'synthDefName<uriPath> and optional auxBusNo'   groupName<opt>
    public static bool createEffect(string nodeName, string uriString, string groupName)
    {
        bool result;
        const string path = "/satie/scene/createEffect";
        OscMessage creationMess = new OscMessage(path);
        string synthName;
        string[] items;
        int auxBusNo = 0;

        items = uriString.Split(' ');

        synthName = items[0];

        //OSCMessage message = new OSCMessage(path);
        creationMess.Add(nodeName);
        creationMess.Add(synthName);
        creationMess.Add(groupName);

        // grab the busAssign number if the string represents a valid integer
        if (items.Length == 2)
        {
            if (int.TryParse(items[1], out auxBusNo))
            {
                creationMess.Add(auxBusNo);
             }
        }
        result = sendOSC(creationMess);

        return result;
    }

    //    /satie/scene createProcess  nodeName  synthDefName<uriPath>   groupName<opt>
    public static bool createProcess(string nodeName, string uriString, string groupName)
    {
        bool result;
        const string path = "/satie/scene/createProcess";
        //OSCMessage message = new OSCMessage(path);
        OscMessage creationMess = new OscMessage(path);
        creationMess.Add(nodeName);
        creationMess.Add(uriString);
        creationMess.Add(groupName);

        result = sendOSC(creationMess);

        return result;
    }

    public static bool createGroup(SATIEgroup group, SATIEnode.satiePluginTypes type)
    {
        
        bool result;
        string path;

        if (type == SATIEnode.satiePluginTypes.sound)
                path = "/satie/scene/createSourceGroup";
        else if (type == SATIEnode.satiePluginTypes.effect)
            path = "/satie/scene/createEffectGroup";
        else if (type == SATIEnode.satiePluginTypes.process)
            path = "/satie/scene/createProcessGroup";        
        else
        {
            Debug.LogError(_instance.GetType() + ".createGroup: BUG FOUND:  type: % not recognized, aborting"+ type);
            return false;
        }
        OscMessage creationMess = new OscMessage(path);
        creationMess.Add(group.nodeName);
        result = sendOSC(creationMess);

        return result;
    }

    public static bool OSCtx(string path, List<object> items)
    {
        OscMessage message = new OscMessage(path);
        bool result;

        // Debug.Log("OSCtx: "+path+"  "+items);

        foreach (object value in items)
        {
            Type t = value.GetType();
            float floatVal;
            string strVal;

            if (t.Equals(typeof(String)))
            {
                strVal = Convert.ToString(value);
                message.Add((string)strVal);
            }
            else
            {
                floatVal = Convert.ToSingle(value);
                message.Add(floatVal);
            }

        }
        result = sendOSC(message);
        return result;
        
    }


    public static void OSCdebug(string path, string val)
    {
        if (!debugMessTx_static)
            return;
        OscMessage message = new OscMessage(path);
        message.Add(val);
        sendOSC(message); 
    }

    public static void OSCdebug(string path, float val)
    {
        if (!debugMessTx_static)
            return;
        OscMessage message = new OscMessage(path);
        message.Add(val);
        sendOSC(message); 
    }

    public static void OSCdebug(string path, List <object> items)
    {
        if (!debugMessTx_static)
            return;
        OscMessage message = new OscMessage(path);

        // Debug.Log("OSCtx: "+path+"  "+items);

        foreach (object value in items)
        {
            Type t = value.GetType();
            float floatVal;
            string strVal;

            if (t.Equals(typeof(String)))
            {
                strVal = Convert.ToString(value);
                message.Add((string)strVal);
            }
            else
            {
                floatVal = Convert.ToSingle(value);
                message.Add(floatVal);
            }

        }
        sendOSC(message);
    }

    public static string LocalIPaddress()
    {
        IPHostEntry host;
        string localIP = "";
        host = Dns.GetHostEntry(Dns.GetHostName());
        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();
                break;
            }
        }
        return localIP;
    }
}


/// the following code needs to be patched into OSCsimpl OscOut.cs
///     void OnDestroy()
/* 
{
    if( isOpen ) Close();
}

public bool Open( int port)
{
    return Open_( port, "");
}


public bool Open( int port, string ipAddress)
{
    return Open_( port, ipAddress);

}

*/