﻿//1
using UnityEditor;
using UnityEngine;
using System.Text.RegularExpressions;

//2
[CustomPropertyDrawer(typeof(satieNodeProperty))]
//3
public class SatieNodePropertyDrawer : PropertyDrawer {

//    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
//    {
//        return 100.0f;
//    }

    public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) {
        // Using BeginProperty / EndProperty on the parent property means that
        // prefab override logic works on the entire property.

        SATIEnode srcNode = property.serializedObject.targetObject as SATIEnode;

        EditorGUI.BeginProperty(position, label, property);

        // Draw label
        position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

        // Don't make child fields be indented
        var indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        // Calculate rects
        var nameRect = new Rect(position.x, position.y, 130, position.height);
        var valueRect = new Rect(position.x + 135, position.y, 130, position.height);
        var buttonRect = new Rect(position.x - 50, position.y, 44, position.height);

        // remove any white spaces in value
        SerializedProperty sp = property.FindPropertyRelative("value");
        string s = Regex.Replace(sp.stringValue, @"\s+", "");
        sp.stringValue = s;

        // remove any white spaces in name
        sp = property.FindPropertyRelative("name");
        s = Regex.Replace(sp.stringValue, @"\s+", "");
        sp.stringValue = s;

        // Draw fields - passs GUIContent.none to each so they are drawn without labels
        EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);
        EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("value"), GUIContent.none);

        //if (newPluginName != "empty") 

        if (GUI.Button(buttonRect, "delete"))
        {
            Debug.Log("DELETE: "+ sp.stringValue);
            srcNode.deleteProperty(sp.stringValue);
            property.serializedObject.ApplyModifiedProperties();
        }

        // Set indent back to what it was
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();
    }
        
}
